const http = require('http');
const rp = require('request-promise');
const apiUrl = 'https://roulette-api.nowakowski-arkadiusz.com';

// Helper to make the code shorter
const post = (path, hashname = '', body = {}) => rp({
    method: 'POST',
    uri: apiUrl + path,
    body: body,
    json: true,
    headers: { 'Authorization': hashname }
});

// Helper to make the code shorter
const get = (path, hashname = '') => rp({
    method: 'GET',
    uri: apiUrl + path,
    json: true,
    headers: { 'Authorization': hashname }
});

test('Player should have 100 chips', () => {
    let hashname;
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return get('/chips', hashname);
    })
    .then(response => expect(response.chips).toEqual(100))
});

const test_func = function(arr, text, path, inn, out){
    test.each(arr)(text, (number) => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets'+path, hashname, { chips: inn }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(out))
    });
}

for (let number of [2,4,6,8,10,11,13,15,17,19,20,22,24,26,29,31,33,35]) {
    test('Black should double the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/black', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(200))
    });
}

test.each([1,3,5,7,9,12,14,16,18,21,23,25,27,28,30,32,34,36])('Bet on red should double the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/red', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(200))
});

test.each([1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35])('Bet on odds should double the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/odd', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(200))
});

test.each([2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36])('Bet on even should double the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/even', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(200))
});

test.each([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18])('Bet on low numbers should double the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/low', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(200))
});

test.each([19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36])('Bet on high numbers should double the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/high', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(200))
});

test.each([1,4,7,10,13,16,19,22,25,28,31,34])('Bet on column1 should triple the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/column/1', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(300))
});

test.each([2,5,8,11,14,17,20,23,26,29,32,35])('Bet on column2 should triple the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/column/2', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(300))
});

test.each([3,6,9,12,15,18,21,24,27,30,33,36])('Bet on column3 should triple the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/column/3', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(300))
});

test.each([1,2,3,4,5,6,7,8,9,10,11,12])('Bet on dozen1 should triple the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/dozen/1', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(300))
});

test.each([2,3,4,5,6,7,8,9,10,11,12,13])('Bet on dozen2 should triple the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/dozen/2', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(300))
});

test.each([3,4,5,6,7,8,9,10,11,12,13,14])('Bet on dozen3 should triple the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/dozen/3', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(300))
});

test.each([1,2,3,4,5,6])('Bet on line 1-2-3-4-5-6 should multiply by 6 the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/line/1-2-3-4-5-6', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(600))
});

test.each([4,5,6,7,8,9])('Bet on line 4-5-6-7-8-9 should multiply by 6 the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/line/4-5-6-7-8-9', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(600))
});

test.each([7,8,9,10,11,12])('Bet on line 7-8-9-10-11-12 should multiply by 6 the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/line/7-8-9-10-11-12', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(600))
});

test.each([10,11,12,13,14,15])('Bet on line 10-11-12-13-14-15 should multiply by 6 the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/line/', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/10-11-12-13-14-15' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(600))
});

test.each([13,14,15,16,17,18])('Bet on line 13-14-15-16-17-18 should multiply by 6 the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/line/13-14-15-16-17-18', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(600))
});

test.each([16,17,18,19,20,21])('Bet on line 16-17-18-19-20-21 should multiply by 6 the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/line/16-17-18-19-20-21', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(600))
});

test.each([19,20,21,22,23,24])('Bet on line 19-20-21-22-23-24 should multiply by 6 the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/line/19-20-21-22-23-24', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(600))
});

test.each([22,23,24,25,26,27])('Bet on line 22-23-24-25-26-27 should multiply by 6 the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/line/22-23-24-25-26-27', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(600))
});

test.each([25,26,27,28,29,30])('Bet on line 25-26-27-28-29-30 should multiply by 6 the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/line/25-26-27-28-29-30', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(600))
});

test.each([28,29,30,31,32,33])('Bet on line 28-29-30-31-32-33 should multiply by 6 the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/line/28-29-30-31-32-33', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(600))
});

test.each([31,32,33,34,35,36])('Bet on line 31-32-33-34-35-36 should multiply by 6 the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/line/31-32-33-34-35-36', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(600))
});


test_func([1,2,4,5], 'Bet on corner 1-2-4-5 should multiply by 9 the number of chips if number %d is spinned', '/corner/1-2-4-5', 100, 900);
test_func([2,3,5,6], 'Bet on corner 2-3-5-6 should multiply by 9 the number of chips if number %d is spinned', '/corner/2-3-5-6', 100, 900);
test_func([4,5,7,8], 'Bet on corner 4-5-7-8 should multiply by 9 the number of chips if number %d is spinned', '/corner/4-5-7-8', 100, 900);
test_func([5,6,8,9], 'Bet on corner 5-6-8-9 should multiply by 9 the number of chips if number %d is spinned', '/corner/5-6-8-9', 100, 900);
test_func([7,8,10,11], 'Bet on corner 7-8-10-11 should multiply by 9 the number of chips if number %d is spinned', '/corner/7-8-10-11', 100, 900);
test_func([8,9,11,12], 'Bet on corner 8-9-11-12 should multiply by 9 the number of chips if number %d is spinned', '/corner/8-9-11-12', 100, 900);
test_func([10,11,13,14], 'Bet on corner 10-11-13-14 should multiply by 9 the number of chips if number %d is spinned', '/corner/10-11-13-14', 100, 900);
test_func([11,12,14,15], 'Bet on corner 11-12-14-15 should multiply by 9 the number of chips if number %d is spinned', '/corner/11-12-14-15', 100, 900);
test_func([13,14,16,17], 'Bet on corner 13-14-16-17 should multiply by 9 the number of chips if number %d is spinned', '/corner/13-14-16-17', 100, 900);
test_func([14,15,17,18], 'Bet on corner 14-15-17-18 should multiply by 9 the number of chips if number %d is spinned', '/corner/14-15-17-18', 100, 900);
test_func([16,17,19,20], 'Bet on corner 16-17-19-20 should multiply by 9 the number of chips if number %d is spinned', '/corner/16-17-19-20', 100, 900);
test_func([17,18,20,21], 'Bet on corner 17-18-20-21 should multiply by 9 the number of chips if number %d is spinned', '/corner/17-18-20-21', 100, 900);
test_func([19,20,22,23], 'Bet on corner 19-20-22-23 should multiply by 9 the number of chips if number %d is spinned', '/corner/19-20-22-23', 100, 900);
test_func([20,21,23,24], 'Bet on corner 20-21-23-24 should multiply by 9 the number of chips if number %d is spinned', '/corner/20-21-23-24', 100, 900);
test_func([22,23,25,26], 'Bet on corner 22-23-25-26 should multiply by 9 the number of chips if number %d is spinned', '/corner/22-23-25-26', 100, 900);
test_func([23,24,26,27], 'Bet on corner 23-24-26-27 should multiply by 9 the number of chips if number %d is spinned', '/corner/23-24-26-27', 100, 900);
test_func([25,26,28,29], 'Bet on corner 25-26-28-29 should multiply by 9 the number of chips if number %d is spinned', '/corner/25-26-28-29', 100, 900);
test_func([26,27,29,30], 'Bet on corner 26-27-29-30 should multiply by 9 the number of chips if number %d is spinned', '/corner/26-27-29-30', 100, 900);
test_func([28,29,31,32], 'Bet on corner 28-29-31-32 should multiply by 9 the number of chips if number %d is spinned', '/corner/28-29-31-32', 100, 900);
test_func([29,30,32,33], 'Bet on corner 29-30-32-33 should multiply by 9 the number of chips if number %d is spinned', '/corner/29-30-32-33', 100, 900);
test_func([31,32,34,35], 'Bet on corner 31-32-34-35 should multiply by 9 the number of chips if number %d is spinned', '/corner/31-32-34-35', 100, 900);
test_func([32,33,35,36], 'Bet on corner 32-33-35-36 should multiply by 9 the number of chips if number %d is spinned', '/corner/32-33-35-36', 100, 900);
test_func([0,1,2,3], 'Bet on corner 0-1-2-3 should multiply by 9 the number of chips if number %d is spinned', '/corner/0-1-2-3', 100, 900);

//straights test
for (i = 0; i <= 36; i++) {
    test_func([i], 'Bet on straight ' + i + ' should multiply by 36 the number of chips if number %d is spinned', '/straight/' + i, 100, 3600);
}

//split test
test_func([0, 2], 'Bet on split 0-2 should multiply by 18 the number of chips if number %d is spinned', '/split/0-2', 100, 1800);
for (i = 0; i < 36; i++) {
    let next = i + 1
    test_func([i, next], 'Bet on split ' + i + '-' + next + ' should multiply by 18 the number of chips if number %d is spinned', '/split/' + i + '-' + next, 100, 1800);
}
for (i = 0; i < 34; i++) {
    let next = i + 3
    test_func([i, next], 'Bet on split ' + i + '-' + next + ' should multiply by 18 the number of chips if number %d is spinned', '/split/' + i + '-' + next, 100, 1800);
}

//street test
test_func([0, 1, 2], 'Bet on split 0-2 should multiply by 12 the number of chips if number %d is spinned', '/street/0-1-2', 100, 1200);
test_func([0, 2, 3], 'Bet on split 0-2 should multiply by 12 the number of chips if number %d is spinned', '/street/0-2-3', 100, 1200);
for (i = 1; i < 35; i += 3) {
    let next = i + 1;
    let next2 = next + 1;
    test_func([i, next, next2], 'Bet on split ' + i + '-' + next + '-' + next2 + ' should multiply by 12 the number of chips if number %d is spinned', '/street/' + i + '-' + next + '-' + next2, 100, 1200);
}

